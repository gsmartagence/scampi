# Select-a11y

Présentation
-------------

**select-a11y** est un script javascript associé à des css qui transforme un ```select``` (multiple ou non) en liste de suggestions avec champ de recherche à l'intérieur de cette liste.

En raison de sa complexité et pour faciliter les contributions, le développement de ce module est déporté sur [un dépôt spécifique](https://gitlab.com/pidila/select-a11y), avec sa propre [page de démonstration](http://pidila.gitlab.io/select-a11y/) et son jeu de tests.

Le script et les css du module de Scampi sont synchronisés sur ce dépôt.

### Accessibilité

Pour la navigation au clavier :
- utilisation d'un élément ```button``` natif, activable au clavier, qui permet d'ouvrir / fermer la liste ;
- le focus est automatiquement déplacé sur le champ de recherche quand on ouvre la liste ;
- les items dans la liste sont atteignables, soit via les flèches directionnelles haut et bas, soit via la tabulation ;
- si l'utilisateur parcourt les items via les flèches, il boucle à l'intérieur de la liste comme dans un menu ;
- si l'utilisateur parcourt les items via la tabulation, après le dernier item, la liste se referme automatiquement et le focus sort de la liste ;
- la liste est refermable via la touche Echap quand le focus est dans la liste et le focus est alors replacé sur le bouton ;
- la sélection d'un item dans la liste peut être effectuée via la touche Espace. La touche Enter, quant à elle, sélectionne et valide le choix. La validation referme la liste et ramène le focus sur le bouton ;
- chacun des items ajoutés contient un élément ```button``` natif, activable au clavier, qui permet de supprimer l'item et qui déplace alors le focus sur l'élément précédent (autre item ajouté ou bouton qui ouvre / ferme la liste).

Pour la restitution par les lecteurs d'écrans :
- une zone masquée visuellement en ```aria-live``` permettant aux lecteurs d'écrans d'annoncer :
  - l'existence et le nombre de suggestions de saisie lors d'une recherche ;
  - de confirmer la bonne sélection d'un élément dans la liste ;
- un attribut ```aria-expanded``` permet d'indiquer l'état ouvert / fermé de la liste ;
- un intitulé et une explication du fonctionnement du composant, qui sont masqués visuellement mais restitués par les lecteurs d'écrans lors de la prise de focus sur le champ de recherche ;
- des attributs ```role="listbox"``` et ```role="option"``` pour une vocalisation des items de la liste ;
- un attribut ```title``` sur les boutons de suppression des items pour préciser le nom de l'item supprimé.

Utilisation
-----------

La présentation de ce module passe par la personnalisation des variables et l'ajout de classes sur les éléments html et bien sûr le script associé.

### Variables sass

```scss
// variables propres au module
$btn-select-a11y-bg:          $gray-9 !default;
$btn-select-a11y-symbol:      "\25BC" !default; // ▼

$tag-item-border:             $gray-7 !default;
$tag-item-supp-symbol:        "\2715" !default; // X
$tag-item-supp-symbol-color:  red !default;
$tag-item-supp-border:        red !default;

$a11y-suggestions-bg:         $gray-10 !default;
$a11y-suggestion-color:       $gray-3 !default;
$a11y-suggestion-color-hover: $gray-1 !default;
$a11y-suggestion-bg-hover:    $gray-9 !default;
$a11y-suggestion-bg-selected: $gray-7 !default;
$a11y-suggestion-border:      $gray-3 !default;
```

### Script

Le script présent dans le module doit être concaténé avec les autres scripts du projet. Si les textes d'aide par défaut ne conviennent pas au projet, ils peuvent être modifiés comme expliqué ci-dessous dans l'exemple.

Pour initier la transformation du ```select```, il faut également ajouter le code suivant dans le fichier javascript du projet. Celui-ci devra impérativement être appelé après le script select-a11y :

```js
var selects = document.querySelectorAll('select[data-select-a11y]');

var selectA11ys = Array.prototype.map.call(selects, function(select){
  return new Select(select);
});
```

La variable ```data-select-a11y``` devra être ajoutée en attribut de la ou les balises ```<select>``` à transformer.

Il est possible de changer les textes par défaut lus par les aides techniques. Lors de la création du select-a11y, il suffit de passer un second paramètre à l'objet via la propriété `text` :

```js
var selects = document.querySelectorAll('select[data-select-a11y]');

var selectA11ys = Array.prototype.map.call(selects, function(select){
  new Select(select, {
    text:{
      help: 'Utilisez la tabulation (ou les touches flèches) pour naviguer dans la liste des suggestions',
      placeholder: 'Rechercher dans la liste',
      noResult: 'Aucun résultat',
      results: '{x} suggestion(s) disponibles',
      deleteItem: 'Supprimer {t}',
      delete: 'Supprimer'
    }
  })
});
```

Les textes ci-dessus sont ceux utilisés par défaut.

Exemple d'utilisation
---------------------

Au sein d'un formulaire, placer un - ou plusieurs - élément ```<select>``` portant en attribut la variable retenue pour le ou les identifier (ici ```data-select-a11y```), ainsi que l'attribut `multiple` s'il y a lieu :

```html

<div class="form-group">
  <label for="select-element">Que voulez-vous faire aujourd'hui ?</label>
  <select class="form-control" id="select-element" data-select-a11y data-placeholder="Chercher dans la liste">
      <option>Dormir</option>
      <option>Grimper aux arbres</option>
      <option>Tricoter</option>
      <option selected>Danser avec les licornes</option>
      <option>Rêver</option>
  </select>
</div>
```