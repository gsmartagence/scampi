/*!
anchor focus
Fixes anchor focus in Chrome/Safari/IE
*/
// by setting the tabindex of the target container to -1 on click/enter
// see -> http://stackoverflow.com/questions/3572843/skip-navigation-link-not-working-in-google-chrome/6188217#6188217

var Scampi = Scampi || {};

Scampi.anchorFocus = function anchorFocus(){
  var matches = Element.prototype.matches || Element.prototype.msMatchesSelector || Element.prototype.webkitMatchesSelector;
  var closest = Element.prototype.closest;

  if (!closest) {
    closest = function(s) {
      var el = this;

      do {
        if (matches.call(el, s)) return el;
        el = el.parentElement || el.parentNode;
      } while (el !== null && el.nodeType === 1);
      return null;
    };
  }

  function focusTarget(hash){
    var splitedHash = hash.split('#');
    var target = document.getElementById(splitedHash[1] || splitedHash[0]);

    if(!target){
      return;
    }

    target.setAttribute('tabindex', -1);
    target.focus();
  }

  if(window.location.hash){
    focusTarget(window.location.hash);
  }

  document.body.addEventListener("click", function(evt){
    var link = closest.call(evt.target, ("a[href^='#']"));

    if(!link){
      return;
    }

    focusTarget(link.href);

  }, true)
}

Scampi.anchorFocus();
