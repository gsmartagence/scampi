# Breadcrumb

Présentation
------------

Le breadcrumb (ou fil d’ariane) est un critère RGAA niveau AAA obligatoire au regard de la Checklist Pidila.

On l’affiche au-dessus du contenu principal. Idéalement on le place en premier élément du main ; ainsi lorsque l’utilisateur utilisera le lien d’accès rapide vers le contenu cet élément sera lu en premier.

Utilisation
------------

Les styles sont portés par la class `breadcrumb`.

### Accessibilité

Un attributs aria-label et un role navigation sur l’élément permettent aux utilisateurs pilotant une aide technique d’être informés de la nature de cet élément.

Les éléments graphiques symbolisant l’arborescence (>, /, ->, etc.) sont insérés soit directement dans la source dans des spans dotés de l’attribut `aria-hidden="true"`, soit placés en css grâce aux pseudo-éléments. Scampi combine ces deux méthodes.

### Configuration

La variable proposée dans ce module est :
- `$breadcrumb-symbol` : caractère de séparation, sa valeur par défaut est `\203A` (soit le caractère ">");

Utilisation
-----------

````html
<nav class="breadcrumb" role="navigation" aria-label="Vous êtes ici : ">
  <ol class="breadcrumb-list">
    <li class="breadcrumb-item"><span aria-hidden="true"></span><a href="/">Accueil</a></li>
    <li class="breadcrumb-item"><span aria-hidden="true"></span><a href="#">Rubrique</a></li>
    <li class="breadcrumb-item"><span aria-hidden="true"></span><a href="#">Sous-rubrique</a></li>
    <li class="breadcrumb-item"><span aria-hidden="true"></span><strong aria-current="page">Titre de la page</strong></li>
  </ol>
</nav>
````
