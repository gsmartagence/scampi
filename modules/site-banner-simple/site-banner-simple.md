# Site-banner-simple

Présentation
-------------

Affichage simple d’une bannière avec logo, titre et baseline.

Utilisation
-------------

### Configuration

Les variables proposées dans ce module sont :

- `$site-title-size-small` : petite taille du titre, sa valeur par défaut est `1.5em`
- `$site-title-size-large` : grande taille du titre, sa valeur par défaut est `2em`
- `$banner-hover-background` : couleur de fond au survol, sa valeur par défaut est `$gray-9`

### Responsive

Sur mobile, la baseline est sous le logo et le titre ; sur les plus grandes résolutions, la baseline se positionne sous le titre et le logo est calé à gauche.


Exemple d’utilisation
-------------

```` html
<div class="container">
  <div class="site-id">
    <img class="site-logo" src="logo.png" alt="">
    <p class="site-title"><a href="#">Ôde à Scampi-Twig</a></p>
    <p class="site-baseline">L'outil de construction de pages statiques qui vous manquait&nbsp;!</p>
  </div>
</div>
````



