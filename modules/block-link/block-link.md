Block-link
=====================================================================


Présentation
---------------------------------------------------------------------

Ce module permet d'étendre la zone cliquable d'un lien.

Ce cas est fréquemment rencontré dans des catalogues (bloc avec image de couverture, titre du livre, prix, etc.) ou dans des listes d'amorces (titre de l'article + début du texte), pour lesquels on souhaite que l'ensemble de la zone se comporte comme un lien.


Utilisation
---------------------------------------------------------------------

Placer uniquement la partie signifiante du lien, celle qu'on souhaite voire restituée par les aides techniques, dans une balise `a`.

Pour que toute la zone soit cliquable on utilise le pseudo élément `:after` sur le lien pour l'étendre sur tout le bloc parent (en position relative), grâce à une position absolute.


### Accessibilité

Le block-link permet d'éviter deux écueils :

- la juxtaposition de liens adjacents conduisant tous vers la même cible ;
- une imbrication dans un élément `<a>` de tous les éléments composant le bloc cliquable, rendant ainsi la restitution vocale "trop bavarde".

Comportement restitué : 

- toute la zone est cliquable, le pointeur correspondant est visible au survol (des css peuvent ajouter un effet visuel pour renforcer cette indication) ;
- la tabulation met le focus sur le lien signifiant (le titre de l'article ou de l'ouvrage dans nos exemples) ;
- à la navigation de lien en lien ou à l'affichage de la liste des liens, la restitution vocale (ou la liste) ne restitue que le lien signifiant.




Exemple d'utilisation
---------------------------------------------------------------------

```html
<div class="block-link">
  <img src="example.org/illustration.png" alt="">
  <h3><a href="example.org">Intitulé du lien</a></h3>
  <p>lorem...</p>
</div>
```
